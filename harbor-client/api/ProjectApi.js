/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/AuditLog', 'model/Errors', 'model/Project', 'model/ProjectDeletable', 'model/ProjectReq', 'model/ProjectSummary'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/AuditLog'), require('../model/Errors'), require('../model/Project'), require('../model/ProjectDeletable'), require('../model/ProjectReq'), require('../model/ProjectSummary'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.ProjectApi = factory(root.HarborApi.ApiClient, root.HarborApi.AuditLog, root.HarborApi.Errors, root.HarborApi.Project, root.HarborApi.ProjectDeletable, root.HarborApi.ProjectReq, root.HarborApi.ProjectSummary);
  }
}(this, function(ApiClient, AuditLog, Errors, Project, ProjectDeletable, ProjectReq, ProjectSummary) {
  'use strict';

  /**
   * Project service.
   * @module api/ProjectApi
   * @version 2.0
   */

  /**
   * Constructs a new ProjectApi. 
   * @alias module:api/ProjectApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the createProject operation.
     * @callback module:api/ProjectApi~createProjectCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Create a new project.
     * This endpoint is for user to create a new project.
     * @param {module:model/ProjectReq} project New created project.
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {Boolean} opts.xResourceNameInLocation The flag to indicate whether to return the name of the resource in Location. When X-Resource-Name-In-Location is true, the Location will return the name of the resource. (default to false)
     * @param {module:api/ProjectApi~createProjectCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.createProject = function(project, opts, callback) {
      opts = opts || {};
      var postBody = project;

      // verify the required parameter 'project' is set
      if (project === undefined || project === null) {
        throw new Error("Missing the required parameter 'project' when calling createProject");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId'],
        'X-Resource-Name-In-Location': opts['xResourceNameInLocation']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = null;

      return this.apiClient.callApi(
        '/projects', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the deleteProject operation.
     * @callback module:api/ProjectApi~deleteProjectCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Delete project by projectID
     * This endpoint is aimed to delete project by project ID.
     * @param {String} projectNameOrId The name or id of the project
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {Boolean} opts.xIsResourceName The flag to indicate whether the parameter which supports both name and id in the path is the name of the resource. When the X-Is-Resource-Name is false and the parameter can be converted to an integer, the parameter will be as an id, otherwise, it will be as a name. (default to false)
     * @param {module:api/ProjectApi~deleteProjectCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.deleteProject = function(projectNameOrId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectNameOrId' is set
      if (projectNameOrId === undefined || projectNameOrId === null) {
        throw new Error("Missing the required parameter 'projectNameOrId' when calling deleteProject");
      }


      var pathParams = {
        'project_name_or_id': projectNameOrId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId'],
        'X-Is-Resource-Name': opts['xIsResourceName']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = null;

      return this.apiClient.callApi(
        '/projects/{project_name_or_id}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getLogs operation.
     * @callback module:api/ProjectApi~getLogsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/AuditLog>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get recent logs of the projects
     * Get recent logs of the projects
     * @param {String} projectName The name of the project
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {String} opts.q Query string to query resources. Supported query patterns are \"exact match(k=v)\", \"fuzzy match(k=~v)\", \"range(k=[min~max])\", \"list with union releationship(k={v1 v2 v3})\" and \"list with intersetion relationship(k=(v1 v2 v3))\". The value of range and list can be string(enclosed by \" or '), integer or time(in format \"2020-04-09 02:36:00\"). All of these query patterns should be put in the query string \"q=xxx\" and splitted by \",\". e.g. q=k1=v1,k2=~v2,k3=[min~max]
     * @param {Number} opts.page The page number (default to 1)
     * @param {Number} opts.pageSize The size of per page (default to 10)
     * @param {module:api/ProjectApi~getLogsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/AuditLog>}
     */
    this.getLogs = function(projectName, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectName' is set
      if (projectName === undefined || projectName === null) {
        throw new Error("Missing the required parameter 'projectName' when calling getLogs");
      }


      var pathParams = {
        'project_name': projectName
      };
      var queryParams = {
        'q': opts['q'],
        'page': opts['page'],
        'page_size': opts['pageSize'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = [AuditLog];

      return this.apiClient.callApi(
        '/projects/{project_name}/logs', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getProject operation.
     * @callback module:api/ProjectApi~getProjectCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Project} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Return specific project detail information
     * This endpoint returns specific project information by project ID.
     * @param {String} projectNameOrId The name or id of the project
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {Boolean} opts.xIsResourceName The flag to indicate whether the parameter which supports both name and id in the path is the name of the resource. When the X-Is-Resource-Name is false and the parameter can be converted to an integer, the parameter will be as an id, otherwise, it will be as a name. (default to false)
     * @param {module:api/ProjectApi~getProjectCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Project}
     */
    this.getProject = function(projectNameOrId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectNameOrId' is set
      if (projectNameOrId === undefined || projectNameOrId === null) {
        throw new Error("Missing the required parameter 'projectNameOrId' when calling getProject");
      }


      var pathParams = {
        'project_name_or_id': projectNameOrId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId'],
        'X-Is-Resource-Name': opts['xIsResourceName']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = Project;

      return this.apiClient.callApi(
        '/projects/{project_name_or_id}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getProjectDeletable operation.
     * @callback module:api/ProjectApi~getProjectDeletableCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProjectDeletable} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get the deletable status of the project
     * Get the deletable status of the project
     * @param {String} projectNameOrId The name or id of the project
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {Boolean} opts.xIsResourceName The flag to indicate whether the parameter which supports both name and id in the path is the name of the resource. When the X-Is-Resource-Name is false and the parameter can be converted to an integer, the parameter will be as an id, otherwise, it will be as a name. (default to false)
     * @param {module:api/ProjectApi~getProjectDeletableCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ProjectDeletable}
     */
    this.getProjectDeletable = function(projectNameOrId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectNameOrId' is set
      if (projectNameOrId === undefined || projectNameOrId === null) {
        throw new Error("Missing the required parameter 'projectNameOrId' when calling getProjectDeletable");
      }


      var pathParams = {
        'project_name_or_id': projectNameOrId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId'],
        'X-Is-Resource-Name': opts['xIsResourceName']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = ProjectDeletable;

      return this.apiClient.callApi(
        '/projects/{project_name_or_id}/_deletable', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the getProjectSummary operation.
     * @callback module:api/ProjectApi~getProjectSummaryCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ProjectSummary} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get summary of the project.
     * Get summary of the project.
     * @param {String} projectNameOrId The name or id of the project
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {Boolean} opts.xIsResourceName The flag to indicate whether the parameter which supports both name and id in the path is the name of the resource. When the X-Is-Resource-Name is false and the parameter can be converted to an integer, the parameter will be as an id, otherwise, it will be as a name. (default to false)
     * @param {module:api/ProjectApi~getProjectSummaryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ProjectSummary}
     */
    this.getProjectSummary = function(projectNameOrId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectNameOrId' is set
      if (projectNameOrId === undefined || projectNameOrId === null) {
        throw new Error("Missing the required parameter 'projectNameOrId' when calling getProjectSummary");
      }


      var pathParams = {
        'project_name_or_id': projectNameOrId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId'],
        'X-Is-Resource-Name': opts['xIsResourceName']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = ProjectSummary;

      return this.apiClient.callApi(
        '/projects/{project_name_or_id}/summary', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the headProject operation.
     * @callback module:api/ProjectApi~headProjectCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Check if the project name user provided already exists.
     * This endpoint is used to check if the project name provided already exist.
     * @param {String} projectName Project name for checking exists.
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {module:api/ProjectApi~headProjectCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.headProject = function(projectName, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectName' is set
      if (projectName === undefined || projectName === null) {
        throw new Error("Missing the required parameter 'projectName' when calling headProject");
      }


      var pathParams = {
      };
      var queryParams = {
        'project_name': projectName,
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = null;

      return this.apiClient.callApi(
        '/projects', 'HEAD',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the listProjects operation.
     * @callback module:api/ProjectApi~listProjectsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/Project>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * List projects
     * This endpoint returns projects created by Harbor.
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {Number} opts.page The page number (default to 1)
     * @param {Number} opts.pageSize The size of per page (default to 10)
     * @param {String} opts.name The name of project.
     * @param {Boolean} opts._public The project is public or private.
     * @param {String} opts.owner The name of project owner.
     * @param {Boolean} opts.withDetail Bool value indicating whether return detailed information of the project (default to true)
     * @param {module:api/ProjectApi~listProjectsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/Project>}
     */
    this.listProjects = function(opts, callback) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'page': opts['page'],
        'page_size': opts['pageSize'],
        'name': opts['name'],
        'public': opts['_public'],
        'owner': opts['owner'],
        'with_detail': opts['withDetail'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = [Project];

      return this.apiClient.callApi(
        '/projects', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the updateProject operation.
     * @callback module:api/ProjectApi~updateProjectCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update properties for a selected project.
     * This endpoint is aimed to update the properties of a project.
     * @param {String} projectNameOrId The name or id of the project
     * @param {module:model/ProjectReq} project Updates of project.
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {Boolean} opts.xIsResourceName The flag to indicate whether the parameter which supports both name and id in the path is the name of the resource. When the X-Is-Resource-Name is false and the parameter can be converted to an integer, the parameter will be as an id, otherwise, it will be as a name. (default to false)
     * @param {module:api/ProjectApi~updateProjectCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.updateProject = function(projectNameOrId, project, opts, callback) {
      opts = opts || {};
      var postBody = project;

      // verify the required parameter 'projectNameOrId' is set
      if (projectNameOrId === undefined || projectNameOrId === null) {
        throw new Error("Missing the required parameter 'projectNameOrId' when calling updateProject");
      }

      // verify the required parameter 'project' is set
      if (project === undefined || project === null) {
        throw new Error("Missing the required parameter 'project' when calling updateProject");
      }


      var pathParams = {
        'project_name_or_id': projectNameOrId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId'],
        'X-Is-Resource-Name': opts['xIsResourceName']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = null;

      return this.apiClient.callApi(
        '/projects/{project_name_or_id}', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
