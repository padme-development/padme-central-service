/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.PingRegistry = factory(root.HarborApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The PingRegistry model module.
   * @module model/PingRegistry
   * @version 2.0
   */

  /**
   * Constructs a new <code>PingRegistry</code>.
   * @alias module:model/PingRegistry
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>PingRegistry</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PingRegistry} obj Optional instance to populate.
   * @return {module:model/PingRegistry} The populated <code>PingRegistry</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('id'))
        obj.id = ApiClient.convertToType(data['id'], 'Number');
      if (data.hasOwnProperty('type'))
        obj.type = ApiClient.convertToType(data['type'], 'String');
      if (data.hasOwnProperty('url'))
        obj.url = ApiClient.convertToType(data['url'], 'String');
      if (data.hasOwnProperty('credential_type'))
        obj.credentialType = ApiClient.convertToType(data['credential_type'], 'String');
      if (data.hasOwnProperty('access_key'))
        obj.accessKey = ApiClient.convertToType(data['access_key'], 'String');
      if (data.hasOwnProperty('access_secret'))
        obj.accessSecret = ApiClient.convertToType(data['access_secret'], 'String');
      if (data.hasOwnProperty('insecure'))
        obj.insecure = ApiClient.convertToType(data['insecure'], 'Boolean');
    }
    return obj;
  }

  /**
   * The ID of the registry
   * @member {Number} id
   */
  exports.prototype.id = undefined;

  /**
   * Type of the registry, e.g. 'harbor'.
   * @member {String} type
   */
  exports.prototype.type = undefined;

  /**
   * The registry address URL string.
   * @member {String} url
   */
  exports.prototype.url = undefined;

  /**
   * Credential type of the registry, e.g. 'basic'.
   * @member {String} credentialType
   */
  exports.prototype.credentialType = undefined;

  /**
   * The registry access key.
   * @member {String} accessKey
   */
  exports.prototype.accessKey = undefined;

  /**
   * The registry access secret.
   * @member {String} accessSecret
   */
  exports.prototype.accessSecret = undefined;

  /**
   * Whether or not the certificate will be verified when Harbor tries to access the server.
   * @member {Boolean} insecure
   */
  exports.prototype.insecure = undefined;


  return exports;

}));
