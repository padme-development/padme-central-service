/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.RobotCreated = factory(root.HarborApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The RobotCreated model module.
   * @module model/RobotCreated
   * @version 2.0
   */

  /**
   * Constructs a new <code>RobotCreated</code>.
   * The response for robot account creation.
   * @alias module:model/RobotCreated
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>RobotCreated</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RobotCreated} obj Optional instance to populate.
   * @return {module:model/RobotCreated} The populated <code>RobotCreated</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('id'))
        obj.id = ApiClient.convertToType(data['id'], 'Number');
      if (data.hasOwnProperty('name'))
        obj.name = ApiClient.convertToType(data['name'], 'String');
      if (data.hasOwnProperty('secret'))
        obj.secret = ApiClient.convertToType(data['secret'], 'String');
      if (data.hasOwnProperty('creation_time'))
        obj.creationTime = ApiClient.convertToType(data['creation_time'], 'Date');
      if (data.hasOwnProperty('expires_at'))
        obj.expiresAt = ApiClient.convertToType(data['expires_at'], 'Number');
    }
    return obj;
  }

  /**
   * The ID of the robot
   * @member {Number} id
   */
  exports.prototype.id = undefined;

  /**
   * The name of the tag
   * @member {String} name
   */
  exports.prototype.name = undefined;

  /**
   * The secret of the robot
   * @member {String} secret
   */
  exports.prototype.secret = undefined;

  /**
   * The creation time of the robot.
   * @member {Date} creationTime
   */
  exports.prototype.creationTime = undefined;

  /**
   * The expiration data of the robot
   * @member {Number} expiresAt
   */
  exports.prototype.expiresAt = undefined;


  return exports;

}));
