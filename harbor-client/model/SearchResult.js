/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ChartVersion'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./ChartVersion'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.SearchResult = factory(root.HarborApi.ApiClient, root.HarborApi.ChartVersion);
  }
}(this, function(ApiClient, ChartVersion) {
  'use strict';

  /**
   * The SearchResult model module.
   * @module model/SearchResult
   * @version 2.0
   */

  /**
   * Constructs a new <code>SearchResult</code>.
   * The chart search result item
   * @alias module:model/SearchResult
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>SearchResult</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/SearchResult} obj Optional instance to populate.
   * @return {module:model/SearchResult} The populated <code>SearchResult</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('name'))
        obj.name = ApiClient.convertToType(data['name'], 'String');
      if (data.hasOwnProperty('score'))
        obj.score = ApiClient.convertToType(data['score'], 'Number');
      if (data.hasOwnProperty('chart'))
        obj.chart = ChartVersion.constructFromObject(data['chart']);
    }
    return obj;
  }

  /**
   * The chart name with repo name
   * @member {String} name
   */
  exports.prototype.name = undefined;

  /**
   * The matched level
   * @member {Number} score
   */
  exports.prototype.score = undefined;

  /**
   * @member {module:model/ChartVersion} chart
   */
  exports.prototype.chart = undefined;


  return exports;

}));
