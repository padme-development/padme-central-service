/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.RetentionSelectorMetadata = factory(root.HarborApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The RetentionSelectorMetadata model module.
   * @module model/RetentionSelectorMetadata
   * @version 2.0
   */

  /**
   * Constructs a new <code>RetentionSelectorMetadata</code>.
   * retention selector
   * @alias module:model/RetentionSelectorMetadata
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>RetentionSelectorMetadata</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RetentionSelectorMetadata} obj Optional instance to populate.
   * @return {module:model/RetentionSelectorMetadata} The populated <code>RetentionSelectorMetadata</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('display_text'))
        obj.displayText = ApiClient.convertToType(data['display_text'], 'String');
      if (data.hasOwnProperty('kind'))
        obj.kind = ApiClient.convertToType(data['kind'], 'String');
      if (data.hasOwnProperty('decorations'))
        obj.decorations = ApiClient.convertToType(data['decorations'], ['String']);
    }
    return obj;
  }

  /**
   * @member {String} displayText
   */
  exports.prototype.displayText = undefined;

  /**
   * @member {String} kind
   */
  exports.prototype.kind = undefined;

  /**
   * @member {Array.<String>} decorations
   */
  exports.prototype.decorations = undefined;


  return exports;

}));
